import com.twitter.scrooge.ScroogeSBT._

com.twitter.scrooge.ScroogeSBT.newSettings

name := "Weed"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.10.3"

com.twitter.scrooge.ScroogeSBT.scroogeBuildOptions in Compile := Seq("-finagle", "-verbose")

resolvers +=
  "Twitter" at "http://maven.twttr.com"

libraryDependencies ++= Seq(
  "com.twitter" %% "finatra" % "1.5.3",
  "com.twitter" % "finagle-thrift_2.10" % "6.13.1",
  "org.apache.thrift" % "libthrift" % "0.8.0",
  "com.twitter" %% "scrooge-core" % "3.13.2",
  "org.codehaus.jackson" % "jackson-mapper-asl" % "1.9.13",
  "mysql"              % "mysql-connector-java"   % "5.1.31",
  "org.json4s"         %% "json4s-jackson"        % "3.2.9",
  "com.google.guava"   % "guava"                  % "16.0.1",
  "org.scalikejdbc"    %% "scalikejdbc"           % "2.1.0",
  "ch.qos.logback"     %  "logback-classic"       % "1.1.2",
  "com.zaxxer"         % "HikariCP"               % "1.4.0"
)
