package jp.bbp.Weed

import java.math.BigInteger
import java.net.URL

import com.fasterxml.jackson.databind.ObjectMapper
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.http.{RequestBuilder, Http}
import com.twitter.finatra._
import com.twitter.finatra.ContentType._
import org.jboss.netty.buffer.ChannelBuffers.wrappedBuffer
import java.security.{SecureRandom, MessageDigest}

import org.jboss.netty.handler.codec.http.{HttpVersion, HttpMethod, DefaultHttpRequest}

object App extends FinatraServer {

  
  class WeedServer extends Controller {

    private val mapper = new ObjectMapper

    private val random:SecureRandom = new SecureRandom

    def generateSessionID():String = {
      new BigInteger(130, random).toString(32)
    }

    get("/") { request =>
      if(!request.cookies.get("WDLSID").equals("true")) redirect("login")
      render.static("index.html").toFuture
    }

    get("/login") { request =>
      render.static("login.html").toFuture
    }

    post("/login") { request =>
      val params = request.params
      if(params.get("user_id") == null || params.get("user_id").toString.length > 0) throw new Exception

      val md = MessageDigest.getInstance("SHA256")

      val id:String = params.get("user_id").toString
      val password = md.digest(params.get("password").toString.getBytes("UTF-8")).map(_&0xFF).map(_.toHexString).mkString

      val client = ClientBuilder().codec(Http()).hosts("localhost:9000").hostConnectionLimit(1).build()

      val bParams = mapper.writeValueAsBytes(Map("user" -> id, "pass" -> password))

      val req = RequestBuilder()
                      .url(new URL("http", "localhost", 9000, "/auth"))
                      .setHeader("Content-Type", "application/json")
                      .setHeader("Content-length", bParams.length.toString)
                      .buildPost(wrappedBuffer(bParams))

      client(req) onSuccess {res =>
        println("got response", res)
      } onFailure { exc =>
        println("failed :-(", exc)
      } ensure {
        print("end")
        client.close()
      }
      // TODO ユーザーサービス作る迄とりあえず適当Cookie直書き あとあとUUIDに変える。
      redirect("/index").cookie("WDLSID", "true").toFuture
    }

    get("/logout") { request =>
      val params = request.params
      if(params.get("user_id") != null && params.get("user_id").toString.length > 0) redirect("/login").cookie("WDLSID", null).toFuture
      else redirect("/login").toFuture
    }

    delete("/photos") { request =>
      render.plain("deleted!").toFuture
    }

    /**
     * User Page
     */
    get("/user/:userId") { request =>
      val username = request.routeParams.getOrElse("userId", null)
      render.plain("hello " + username).toFuture
    }

    /**
     * Rendering json
     *
     * curl -I http://localhost:7070/data.json => "{foo:bar}"
     */
    get("/api/:version/data.json") { request =>
      val apiVerdion = request.routeParams.getOrElse("version", "v1")
      render.json(Map("foo" -> "bar")).toFuture
    }

    /**
     * Query params
     *
     * curl http://localhost:7070/search?q=foo => "no results for foo"
     */
    get("/search") { request =>
      request.params.get("q") match {
        case Some(q) => render.plain("no results for "+ q).toFuture
        case None    => render.plain("query param q needed").status(500).toFuture
      }
    }

    /**
     * Redirects
     *
     * curl http://localhost:7070/redirect
     */
    get("/redirect") { request =>
      redirect("http://localhost:7070/", permanent = true).toFuture
    }

    /**
     * Uploading files
     *
     * curl -F avatar=@/path/to/img http://localhost:7070/profile
     */
    post("/profile") { request =>
      request.multiParams.get("avatar").map { avatar =>
        println("content type is " + avatar.contentType)
        avatar.writeToFile("/tmp/avatar") //writes uploaded avatar to /tmp/avatar
      }
      render.plain("ok").toFuture
    }

    options("/some/resource") { request =>
      render.plain("usage description").toFuture
    }

    /**
     * Rendering views
     *
     * curl http://localhost:7070/template
     */
    class AnView extends View {
      val template = "an_view.mustache"
      val some_val = "random value here"
    }

    get("/template") { request =>
      val anView = new AnView
      render.view(anView).toFuture
    }


    /**
     * Custom Error Handling
     *
     * curl http://localhost:7070/error
     */
    get("/error")   { request =>
      1234/0
      render.plain("we never make it here").toFuture
    }

    /**
     * Custom Error Handling with custom Exception
     *
     * curl http://localhost:7070/unauthorized
     */
    class Unauthorized extends Exception

    get("/unauthorized") { request =>
      throw new Unauthorized
    }

    error { request =>
      request.error match {
        case Some(e:ArithmeticException) =>
          render.status(500).plain("whoops, divide by zero!").toFuture
        case Some(e:Unauthorized) =>
          render.status(401).plain("Not Authorized!").toFuture
        case Some(e:UnsupportedMediaType) =>
          render.status(415).plain("Unsupported Media Type!").toFuture
        case _ =>
          render.status(500).plain("Something went wrong!").toFuture
      }
    }


    /**
     * Custom 404s
     *
     * curl http://localhost:7070/notfound
     */
    notFound { request =>
      render.status(404).plain("not found yo").toFuture
    }

    /**
     * Arbitrary Dispatch
     *
     * curl http://localhost:7070/go_home
     */
    get("/go_home") { request =>
      route.get("/")
    }

    get("/search_for_dogs") { request =>
      route.get("/search", Map("q" -> "dogs"))
    }

    get("/delete_photos") { request =>
      route.delete("/photos")
    }

    get("/gif") { request =>
      render.static("/dealwithit.gif").toFuture
    }

    /**
     * Dispatch based on Content-Type
     *
     * curl http://localhost:7070/blog/index.json
     * curl http://localhost:7070/blog/index.html
     */
    get("/blog/index.:format") { request =>
      respondTo(request) {
        case _:Html => render.html("<h1>Hello</h1>").toFuture
        case _:Json => render.json(Map("value" -> "hello")).toFuture
      }
    }

    /**
     * Also works without :format route using browser Accept header
     *
     * curl -H "Accept: text/html" http://localhost:7070/another/page
     * curl -H "Accept: application/json" http://localhost:7070/another/page
     * curl -H "Accept: foo/bar" http://localhost:7070/another/page
     */

    get("/another/page") { request =>
      respondTo(request) {
        case _:Html => render.plain("an html response").toFuture
        case _:Json => render.plain("an json response").toFuture
        case _:All => render.plain("default fallback response").toFuture
      }
    }

    /**
     * Metrics are supported out of the box via Twitter's Ostrich library.
     * More details here: https://github.com/twitter/ostrich
     *
     * curl http://localhost:7070/slow_thing
     *
     * By default a stats server is started on 9990:
     *
     * curl http://localhost:9990/stats.txt
     *
     */

    get("/slow_thing") { request =>
      stats.counter("slow_thing").incr
      stats.time("slow_thing time") {
        Thread.sleep(100)
      }
      render.plain("slow").toFuture
    }

  }

  register(new WeedServer())
}
